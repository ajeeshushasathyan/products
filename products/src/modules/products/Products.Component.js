import React from 'react';
import { Dropdown } from 'primereact/dropdown';
import { InputText } from 'primereact/inputtext';
import '../../assets/css/products.css';
import { Button } from 'primereact/button';
import { currencyConversionValue, getPercentage, getSearchResult } from '../../utils/utils';
const commonCurrencies = require('../../assets/data/common-currencies.json');

const ProductsComponent = (props) => {
    const { categoryProducts, currency, currencyConversion } = props;
    const [searchText, setSearchText] = React.useState('');
    const [selectedCategory, setSelectedCategory] = React.useState(categoryProducts[0]);
    const [selectedCurrency, setSelectedCurrency] = React.useState(currency[0]);
    const getPrice = (data, price) => {
        let priceTemp = price;
        if (selectedCurrency !== data.currency) {
            priceTemp = currencyConversionValue(currencyConversion.data, priceTemp, data.currency, selectedCurrency)
        }
        return priceTemp;
    }
    const getChangeData = (value, selectedCategoryTemp = selectedCategory) => {
        selectedCategoryTemp.search_result = value === '' ? '' : getSearchResult(selectedCategoryTemp.data, ['title', 'description'], value)
        setSelectedCategory(selectedCategoryTemp);
    }
    const getProductItemTemplate = (selectedCategoryData) => {
        console.log("selectedCategoryData", selectedCategoryData)
        return (
            selectedCategoryData.map((data) => (
                <div key={data.article_number} className="p-sm-12 p-md-6 p-lg-4 p-xl-4"><div className="product-grid-item card w-95" onClick={() => window.open(data.url, '_blank').focus()}>
                    <div className="product-grid-item-top">
                        <div>
                            <span className="product-category">{data.category}</span>
                        </div>
                        <span>Article number: {data.article_number}</span>
                    </div>
                    <div className="product-grid-item-content">
                        {data.image && <img src={`${data.image}`} onError={(e) => e.target.src = 'https://www.primefaces.org/wp-content/uploads/2020/05/placeholder.png'} alt={data.name} />}
                        <div className={`${data.image ? '' : 'p-mt-3'} product-name`}>{data.title}</div>
                        <div className="product-description">{data.description}</div>
                    </div>
                    <div className="product-grid-item-bottom">
                        <span className="product-price">{`${commonCurrencies[selectedCurrency].symbol} ${getPrice(data, data.price_campaign ? data.price_campaign : data.price)}`}
                            {data.price_campaign && <span className="product-price-sub p-ml-2">{`${commonCurrencies[selectedCurrency].symbol} ${getPrice(data, data.price)}`}
                            </span>}{data.price_campaign && <span className="product-price-off p-ml-1">{`${getPercentage(data.price_campaign, data.price)}% off`}</span>}
                        </span>
                        <Button icon="pi pi-shopping-cart" className={`p-button-sm ${data.in_stock ? '' : 'p-button-danger'}`} label={`${!data.in_stock ? 'Out of stock' : 'Add to Cart'}`} disabled={!data.in_stock}></Button>
                    </div>
                </div></div>
            ))
        )
    }
    return (
        <>
            <div className="p-p-2">
                <div className="p-grid p-shadow-2 card sticky-header">
                    <div className="p-col"><Dropdown className="p-mt-2 product-header-items" optionLabel="category" value={selectedCategory} options={categoryProducts} onChange={(e) => {
                        debugger
                        getChangeData('', { ...e.value })
                    }} />
                        <InputText className="product-header-items" placeholder="Search" value={searchText} type="text" onChange={(e) => { setSearchText(e.target.value); getChangeData(e.target.value); }} /></div>
                    <div className="p-col-fixed" style={{ width: '120px' }}><Dropdown className="p-mt-2" value={selectedCurrency} options={currency} onChange={(e) => setSelectedCurrency(e.value)} /></div>
                </div>
                <div className="p-grid">
                    {
                        getProductItemTemplate(selectedCategory.search_result ? selectedCategory.search_result : selectedCategory.data)
                    }
                </div>

            </div>
        </>
    );
}
export default ProductsComponent