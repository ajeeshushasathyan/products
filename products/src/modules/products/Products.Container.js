import React from 'react';
import { CURRENCY_API } from '../../config';
import ProductsComponent from './Products.Component';
const products = require('../../assets/data/articles.json');
const ProductContainer = () => {
    const [categoryProducts, setCategoryProducts] = React.useState();
    const [currency, setCurrency] = React.useState();
    const [currencyConversion, setCurrencyConversion] = React.useState();
    const getCurrentConversionValue = () => {
        fetch(CURRENCY_API).then(response =>
            response.json().then(data => ({
                data: data,
                status: response.status
            })
            ).then(res => {
                setCurrencyConversion(res);
            }));
    }

    React.useEffect(() => {
        console.log(products);
        getCurrentConversionValue();
        let currencyTemp = [];
        let category = [{ category: 'Products', category_id: 1, data: products, search_result: '' }];
        let categoryIndex;
        products.forEach((item, index) => {
            categoryIndex = category.findIndex(cat => cat.category === item.category);
            if (categoryIndex === -1) {
                category.push({ category: item.category, category_id: category.length + 1, data: [{ ...item }], search_result: '' });
            } else {
                category[categoryIndex] = { ...category[categoryIndex], data: [...category[categoryIndex].data, item] }
            }
            currencyTemp.indexOf(item.currency) === -1 && currencyTemp.push(item.currency);
        })
        setCurrency(currencyTemp);
        setCategoryProducts(category);
        console.log(category)
    }, []);
    return (
        <>{categoryProducts && currency && currencyConversion && <ProductsComponent {...{ categoryProducts, currency, currencyConversion }} />}</>
    )
}
export default ProductContainer;