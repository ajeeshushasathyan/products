
// display results after convertion
export const currencyConversionValue = (currency, value, resultFrom, resultTo) => {
    let fromRate = currency.rates[resultFrom];
    let toRate = currency.rates[resultTo];
    return ((toRate / fromRate) * value).toFixed(2);
}
export const getSearchResult = (source, searchFields, searchVal='') => {
    var results = [];
    for (var i = 0; i < source.length; i++) {
        searchFields.forEach(item => {
            console.log(source[i][item])
            if (source[i][item] && ((source[i][item]).toString()).toLowerCase().includes(searchVal.toLowerCase())) {
                results.push(source[i]);
            }
        })
        
    }
    return results;
}
export const getPercentage = (partialValue, totalValue) => {
    return ((100 * partialValue) / totalValue).toFixed(0);
 } 