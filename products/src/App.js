import './App.css';
import ProductContainer from './modules/products/Products.Container';

function App() {
  return (
    <ProductContainer/>
  );
}

export default App;
